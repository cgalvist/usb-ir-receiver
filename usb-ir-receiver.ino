#include "TrinketHidCombo.h"
#include <avr/delay.h>

// Remote control libraries
#include "src/data/rc-codes/akb33659510/rc-keys-translate.h"
// #include "src/data/rc-codes/tv-box/rc-keys-translate.h"
// #include "src/data/rc-codes/tv-decoder/rc-keys-translate.h"

// Custom libraries
#include "src/keyboard/keyboard.h"
#include "src/mouse/mouse.h"

// **** TSOP is connected to port PB2 **** //////
#define REPEAT_DELAY 220

// Set to 0 after finding your codes
#define DEBUG 0

volatile uint8_t m = 0, tcnt = 0, startflag = 0;
uint32_t irdata = 0, keydata = 0;
bool enabledMouse = false;
bool pressed = false;
bool complete = false;
int lastMouseX = 0, lastMouseY = 0;

void setup() {
  // P1 (LED) OUT not used in sketch
  DDRB |= (1 << DDB1);
  // A PB2 lift will not hurt.
  PORTB |= 1 << PB2;
  // Interrupt int0 enable
  GIMSK |= 1 << INT0;
  // Any logical change on INT0 generates an interrupt request
  MCUCR |= 1 << ISC00;
  GTCCR |= 1 << PSR0;
  TCCR0A = 0;
  // Divider /1024
  TCCR0B = (1 << CS02) | (1 << CS00);
  // Interrupt Timer/Counter1 Overflow  enable
  TIMSK = 1 << TOIE0;

  // Start the USB device engine and enumerate
  TrinketHidCombo.begin();
}

void loop() {
  // If a code has been received
  if (complete) {
    // If a code is new
    if (keydata != 0) {
      action(keydata);
      pressed = true;
    } else {
      Mouse::mouseAcceleration(enabledMouse, lastMouseX, lastMouseY);
    }

    Mouse::mouseMove(lastMouseX, lastMouseY);

    complete = false;
    // to balance repeating/input delay of the remote
    ms_delay(REPEAT_DELAY);

  } else if (pressed) {
    digitalWrite(1, LOW);
    if (enabledMouse)
      Mouse::mouseMove(0, 0);
    else
      Keyboard::pressSingleKey(0);
    pressed = false;
  } else {
    // restrain USB polling on empty cycles
    _delay_ms(1);
    // check if USB needs anything done
    TrinketHidCombo.poll();
  }
}

/**
  Interruption Service Routine ("External Interrupt Request 0")
  https://medium.com/@togunchan/interrupts-in-arduino-types-features-and-usage-explained-aaa36a658f28
 */
ISR(INT0_vect) {
  // If log1
  if (PINB & 1 << 2) {
    TCNT0 = 0;
  } else {
    // If log0
    tcnt = TCNT0;
    if (startflag) {
      if (30 > tcnt && tcnt > 2) {
        if (tcnt > 15 && m < 32) {
          irdata |= (2147483648 >> m);
        }
        m++;
      }
    } else {
      startflag = 1;
    }
  }
}

/**
  Interruption Service Routine ("Timer/Counter0 Overflow")
  https://medium.com/@togunchan/interrupts-in-arduino-types-features-and-usage-explained-aaa36a658f28
 */
ISR(TIMER0_OVF_vect) {
  if (m)
    complete = true;
  m = 0;
  startflag = 0;
  keydata = irdata;

  // if the index is not 0, then create an end flag
  irdata = 0;
}

/**
  USB polling delay function
 */
void ms_delay(uint16_t x) {
  for (uint16_t m = 0; m < (x / 10); m++) {
    _delay_ms(10);
    TrinketHidCombo.poll();
  }
}

/**
  Remote key action
  @keycode Received key code
 */
void action(uint32_t keycode) {
  switch (keycode) {
  case KEY_MOUSE_SWITCH:
    Mouse::enableMouseMode(enabledMouse, lastMouseX, lastMouseY);
    break;
  case KEY_OK:
    enabledMouse ? Mouse::mouseAction(ClickLeft, lastMouseX, lastMouseY)
                 : Keyboard::pressSingleKey(KEYCODE_ENTER);
    break;
  case KEY_MENU:
    enabledMouse ? Mouse::mouseAction(ClickRight, lastMouseX, lastMouseY)
                 : Keyboard::pressSingleKey(KEYCODE_APP);
    break;
  case KEY_UP:
    enabledMouse ? Mouse::mouseAction(MoveUp, lastMouseX, lastMouseY)
                 : Keyboard::pressSingleKey(KEYCODE_ARROW_UP);
    break;
  case KEY_DOWN:
    enabledMouse ? Mouse::mouseAction(MoveDown, lastMouseX, lastMouseY)
                 : Keyboard::pressSingleKey(KEYCODE_ARROW_DOWN);
    break;
  case KEY_LEFT:
    enabledMouse ? Mouse::mouseAction(MoveLeft, lastMouseX, lastMouseY)
                 : Keyboard::pressSingleKey(KEYCODE_ARROW_LEFT);
    break;
  case KEY_RIGHT:
    enabledMouse ? Mouse::mouseAction(MoveRight, lastMouseX, lastMouseY)
                 : Keyboard::pressSingleKey(KEYCODE_ARROW_RIGHT);
    break;
  case KEY_POWER:
    Keyboard::pressModifierKey(KEYCODE_MOD_LEFT_GUI);
    break;
  case KEY_RETURN:
    Keyboard::pressSingleKey(KEYCODE_BACKSPACE);
    break;
  case KEY_TAB:
    Keyboard::pressSingleKey(KEYCODE_TAB);
    break;
  case KEY_ESC:
    Keyboard::pressSingleKey(KEYCODE_ESC);
    break;
  case KEY_PLAY:
  case KEY_PAUSE:
    Keyboard::pressMultimediaKey(MMKEY_PLAYPAUSE);
    break;
  case KEY_STOP:
    Keyboard::pressMultimediaKey(MMKEY_STOP);
    break;
  case KEY_VOL_UP:
    Keyboard::pressMultimediaKey(MMKEY_VOL_UP);
    break;
  case KEY_VOL_DOWN:
    Keyboard::pressMultimediaKey(MMKEY_VOL_DOWN);
    break;
  case KEY_MUTE:
    Keyboard::pressMultimediaKey(MMKEY_MUTE);
    break;
  case KEY_PREV:
    Keyboard::pressMultimediaKey(MMKEY_SCAN_PREV_TRACK);
    break;
  case KEY_NEXT:
    Keyboard::pressMultimediaKey(MMKEY_SCAN_NEXT_TRACK);
    break;
  case KEY_LOCK:
    Keyboard::pressModifierCombo(KEYCODE_MOD_LEFT_GUI, KEYCODE_L);
    break;
  case KEY_FULL_SCREEN:
    Keyboard::pressSingleKey(KEYCODE_F11);
    break;
  case KEY_1:
    Keyboard::pressSingleKey(KEYCODE_1);
    break;
  case KEY_2:
    Keyboard::pressSingleKey(KEYCODE_2);
    break;
  case KEY_3:
    Keyboard::pressSingleKey(KEYCODE_3);
    break;
  case KEY_4:
    Keyboard::pressSingleKey(KEYCODE_4);
    break;
  case KEY_5:
    Keyboard::pressSingleKey(KEYCODE_5);
    break;
  case KEY_6:
    Keyboard::pressSingleKey(KEYCODE_6);
    break;
  case KEY_7:
    Keyboard::pressSingleKey(KEYCODE_7);
    break;
  case KEY_8:
    Keyboard::pressSingleKey(KEYCODE_8);
    break;
  case KEY_9:
    Keyboard::pressSingleKey(KEYCODE_9);
    break;
  case KEY_0:
    Keyboard::pressSingleKey(KEYCODE_0);
    break;

  default:
    if (DEBUG)
      Keyboard::println(keydata);
    else
      return;
  }
  digitalWrite(1, HIGH);
}
