# USB Infrared Receiver

Infrared receiver as a keyboard with `Digispark attiny85`.

With this development it is possible to control different devices (PCs, video game consoles, Smartphones) by means of an infrared remote control.

Code modified from the original taken from `MatMK` at [instructables.com](https://www.instructables.com/Digispark-IR-Receiver-Keyboard/).

> **NOTE:** This code has been created to work with remote controls that use the `NEC` protocol. With other protocols the code may not work.

## Requisites

- `Arduino IDE v2` or `Arduino-CLI`
- `Digispark Attiny85`
- `An infrared remote control working with NEC protocol`

## Remote control codes

> If you use a remote control from another reference or brand, you can modify the `DEBUG` variable in the code to the value 1 and then compile. Open a plain text file and pressing the buttons on the remote will write the hexadecimal code of the keys pressed.

> You can add your custom remote control adding the codes in the path `src/data/rc-codes/YOUR-REMOTE-SERIAL-CODE-OR-NAME/`, making your own `rc-keys.h` and `rc-keys-translate.h` files and change the `rc-keys-translate.h` library in the `usb-ir-receiver.ino` file.

- [LG DVD (AKB33659510)](src/data/rc-codes/akb33659510/rc-keys.h)
- [Generic TV Box](src/data/rc-codes/tv-box/rc-keys.h)
- [Generic TV Digital Decoder](src/data/rc-codes/tv-decoder/rc-keys.h)

## Issues

- The key repeat function doesn't work on some remote controls

## Development Environment

Steps for `Ubuntu 24.04`.

### Install general dependencies

#### Install libusb

```sh
sudo apt -y install libusb-0.1-4
```

#### Install TrinketHidCombo library

```sh
cd /tmp/
git clone https://github.com/adafruit/Adafruit-Trinket-USB/
cd Adafruit-Trinket-USB/
mkdir -p ~/.arduino15/libraries/
cp -r TrinketHidCombo/ ~/.arduino15/libraries/
rm -rf /tmp/Adafruit-Trinket-USB/
```

#### Fix permissions problem

> Code taken from [instructables.com](https://www.instructables.com/Getting-Started-With-MH-ET-LIVE-Tiny88-Arduino-Boa/)

```sh
cd /tmp/
wget https://raw.githubusercontent.com/micronucleus/micronucleus/master/commandline/49-micronucleus.rules
sudo mv 49-micronucleus.rules /etc/udev/rules.d
sudo udevadm control --reload-rules && sudo udevadm trigger
```

### Arduino IDE

> Steps taken from [The Electronics Guy](https://www.youtube.com/watch?v=JGf6P52LO5c)

In `Arduino IDE`, you need to configure additional boards to work with `Digispark Attiny85`:

Go to `File` -> `Preferences` -> `Additional boards manager URLs:` and add a new line with the following content and save the changes:

`https://raw.githubusercontent.com/digistump/arduino-boards-index/master/package_digistump_index.json`

Then go to `Tools` -> `Board` -> `Boards Manager` and install the `Digistump AVR Boards` package.

Then we select the board with `Tools` -> `Board` -> `Digistump AVR Boards` -> `Digispark (default - 16.5 mhz)`

#### Update microkernel for `attiny85`:

> Code taken from [instructables.com](https://www.instructables.com/Getting-Started-With-MH-ET-LIVE-Tiny88-Arduino-Boa/) 

```sh
sudo apt -y install libusb-dev 
cd /tmp/ 
git clone https://github.com/micronucleus/micronucleus 
cd micronucleus/commandline 
make 
mv ~/.arduino15/packages/digistump/tools/micro nucleus/2.0a4/micronucleus ~/.arduino15/packages/digistump/tools/micronucleus/2.0a4/micronucleus.backup 
cp ./micronucleus ~/.arduino15/packages/digistump/tools/micronucleus/2.0a4/ 
rm -rf /tmp/micronucleus/
```

### Arduino-CLI

Install board

```sh
# Update database
arduino-cli core update-index
# Add support for attiny boards
arduino-cli config add board_manager.additional_urls https://raw.githubusercontent.com/digistump/arduino-boards-index/master/package_digistump_index.json
# Install core (digistump:avr)
arduino-cli core install digistump:avr
```

Compile project

```sh
make
```