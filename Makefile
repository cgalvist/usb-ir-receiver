# Original code from https://www.youtube.com/watch?v=3gvpBkOQR0g

BOARD?=digistump:avr:digispark-tiny
PORT?=/dev/ttyUSB0
BUILD=build
LIBRARIES=~/.arduino15/libraries

.PHONY: default lint all flash clean

default: lint all flash clean

lint:
	cpplint --extensions=ino --filter=-whitespace,-legal/copyright *.ino

all:
	arduino-cli compile --fqbn $(BOARD) --output-dir $(BUILD) --libraries $(LIBRARIES) ./

flash:
	arduino-cli upload --fqbn $(BOARD) --port $(PORT) --input-dir $(BUILD)

clean:
	rm -rf build