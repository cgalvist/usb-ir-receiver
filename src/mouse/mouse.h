#define MOUSE_SENSITIVITY 1

enum MouseEvent {
  ClickRight,
  ClickLeft,
  MoveUp,
  MoveDown,
  MoveLeft,
  MoveRight
};

namespace Mouse {
void enableMouseMode(bool &enabledMouse, int &lastMouseX, int &lastMouseY);

void mouseAcceleration(bool enabledMouse, int &lastMouseX, int &lastMouseY);

void mouseAction(MouseEvent event, int &lastMouseX, int &lastMouseY);

void mouseMove(int x, int y);
} // namespace Mouse