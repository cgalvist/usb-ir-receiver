#include "mouse.h"
#include "TrinketHidCombo.h"

namespace Mouse {
/**
Enable or disable mouse mode
*/
void enableMouseMode(bool &enabledMouse, int &lastMouseX, int &lastMouseY) {
  enabledMouse = !enabledMouse;
  lastMouseX = 0;
  lastMouseY = 0;
}

/**
    Make mouse movements accelerate
 */
void mouseAcceleration(bool enabledMouse, int &lastMouseX, int &lastMouseY) {
  if (enabledMouse) {
    lastMouseX *= 2;
    if (lastMouseX > 64)
      lastMouseX = 64;
    else if (lastMouseX < -64)
      lastMouseX = -64;
    lastMouseY *= 2;
    if (lastMouseY > 64)
      lastMouseY = 64;
    else if (lastMouseY < -64)
      lastMouseY = -64;
  }
}

/**
    Execute mouse action
 */
void mouseAction(MouseEvent event, int &lastMouseX, int &lastMouseY) {
  switch (event) {
  case ClickRight:
    TrinketHidCombo.mouseMove(lastMouseX, lastMouseY, MOUSEBTN_RIGHT_MASK);
    break;
  case ClickLeft:
    TrinketHidCombo.mouseMove(lastMouseX, lastMouseY, MOUSEBTN_LEFT_MASK);
    break;
  case MoveUp:
    lastMouseX = 0;
    lastMouseY = -MOUSE_SENSITIVITY;
    break;
  case MoveDown:
    lastMouseX = 0;
    lastMouseY = MOUSE_SENSITIVITY;
    break;
  case MoveLeft:
    lastMouseX = -MOUSE_SENSITIVITY;
    lastMouseY = 0;
    break;
  case MoveRight:
    lastMouseX = MOUSE_SENSITIVITY;
    lastMouseY = 0;
    break;
  }

  if (event == ClickLeft || event == ClickRight) {
    lastMouseX = 0;
    lastMouseY = 0;
  }

  if (event == MoveUp || event == MoveDown || event == MoveLeft ||
      event == MoveRight) {
    mouseMove(lastMouseX, lastMouseY);
  }
}

/**
    Move mouse pointer to specified coordinates
 */
void mouseMove(int x, int y) { TrinketHidCombo.mouseMove(x, y, 0); }
} // namespace Mouse