#include "keyboard.h"
#include "TrinketHidCombo.h"

namespace Keyboard {
/**
Press single key
*/
void pressSingleKey(uint8_t keyCode) { TrinketHidCombo.pressKey(0, keyCode); }

/**
Press multimedia key
*/
void pressMultimediaKey(uint8_t keyCode) {
  TrinketHidCombo.pressMultimediaKey(keyCode);
}

/**
Press single modifier key
*/
void pressModifierKey(uint8_t keyCode) { TrinketHidCombo.pressKey(keyCode, 0); }

/**
Press combo with modifier key(s)
*/
void pressModifierCombo(uint8_t modifiers, uint8_t keyCodes) {
  TrinketHidCombo.pressKey(modifiers, keyCodes);
}

/**
Print key code in hex format
*/
void println(uint32_t keyData) { TrinketHidCombo.println(keyData, HEX); }
} // namespace Keyboard