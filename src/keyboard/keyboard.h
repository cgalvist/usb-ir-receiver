#include <stdint.h>

namespace Keyboard {
void pressSingleKey(uint8_t keyCode);

void pressMultimediaKey(uint8_t keyCode);

void pressModifierKey(uint8_t keyCode);

void pressModifierCombo(uint8_t modifiers, uint8_t keyCodes);

void println(uint32_t keyData);
} // namespace Keyboard