// Remote keys translation for main code

#include "rc-keys.h"

// Custom keys
#define KEY_MOUSE_SWITCH REMOTE_SHIFT
#define KEY_YOUTUBE REMOTE_YOUTUBE
#define KEY_NETFLIX REMOTE_NETFLIX
#define KEY_MEDIA REMOTE_MEDIA
#define KEY_HELP REMOTE_HELP

// Navigation keys
#define KEY_POWER REMOTE_POWER
#define KEY_UP REMOTE_UP
#define KEY_DOWN REMOTE_DOWN
#define KEY_LEFT REMOTE_LEFT
#define KEY_RIGHT REMOTE_RIGHT
#define KEY_OK REMOTE_OK
#define KEY_RETURN REMOTE_DELETE
#define KEY_TAB REMOTE_FN
#define KEY_BACK REMOTE_BACK
#define KEY_MENU REMOTE_MENU
#define KEY_HOME REMOTE_HOME

// Multimedia keys
#define KEY_VOL_UP REMOTE_VOL_UP
#define KEY_VOL_DOWN REMOTE_VOL_DOWN

// Numeric keys
#define KEY_1 REMOTE_KEY_1
#define KEY_2 REMOTE_KEY_2
#define KEY_3 REMOTE_KEY_3
#define KEY_4 REMOTE_KEY_4
#define KEY_5 REMOTE_KEY_5
#define KEY_6 REMOTE_KEY_6
#define KEY_7 REMOTE_KEY_7
#define KEY_8 REMOTE_KEY_8
#define KEY_9 REMOTE_KEY_9
#define KEY_0 REMOTE_KEY_0

// Unused keys
#define KEY_ESC 0x01
#define KEY_PLAY 0x02
#define KEY_PAUSE 0x03
#define KEY_STOP 0x04
#define KEY_MUTE 0x05
#define KEY_NEXT 0x06
#define KEY_PREV 0x07
#define KEY_LOCK 0x08
#define KEY_FULL_SCREEN 0x09