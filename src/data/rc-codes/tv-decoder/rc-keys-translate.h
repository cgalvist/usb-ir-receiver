// Remote keys translation for main code

#include "rc-keys.h"

// Custom keys
#define KEY_MOUSE_SWITCH REMOTE_GOTO

// Navigation keys
#define KEY_POWER REMOTE_POWER
#define KEY_UP REMOTE_UP
#define KEY_DOWN REMOTE_DOWN
#define KEY_LEFT REMOTE_LEFT
#define KEY_RIGHT REMOTE_RIGHT
#define KEY_OK REMOTE_OK
#define KEY_RETURN REMOTE_FAVORITES
#define KEY_TAB REMOTE_TTX
#define KEY_ESC REMOTE_EXIT
#define KEY_MENU REMOTE_MENU

// Multimedia keys
#define KEY_PLAY REMOTE_PLAY
#define KEY_PAUSE REMOTE_PAUSE
#define KEY_STOP REMOTE_STOP
#define KEY_NEXT REMOTE_SKIP_AFTER
#define KEY_PREV REMOTE_SKIP_BEFORE
#define KEY_VOL_UP REMOTE_PAGE_UP
#define KEY_VOL_DOWN REMOTE_PAGE_DOWN
#define KEY_MUTE REMOTE_VOL_MUTE

// Keys for operative system actions
#define KEY_LOCK REMOTE_PVR
#define KEY_FULL_SCREEN REMOTE_V_FORMAT

// Numeric keys
#define KEY_1 REMOTE_KEY_1
#define KEY_2 REMOTE_KEY_2
#define KEY_3 REMOTE_KEY_3
#define KEY_4 REMOTE_KEY_4
#define KEY_5 REMOTE_KEY_5
#define KEY_6 REMOTE_KEY_6
#define KEY_7 REMOTE_KEY_7
#define KEY_8 REMOTE_KEY_8
#define KEY_9 REMOTE_KEY_9
#define KEY_0 REMOTE_KEY_0

// Unused keys